#!/bin/bash
# use doas if installed

if [ -x "$(command -v doas)" ] && [ -e /etc/doas.conf ]; then
	ld="doas"
else
	ld="sudo"
fi

# install wget and unzip if not installed
[ -x "$(command -v wget)" ] || $ld apt install wget -yy
[ -x "$(command -v unzip)" ] || $ld apt install unzip -yy

nfdir="$HOME/.local/share/fonts/nerdfonts"
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/JetBrainsMono.zip

mkdir -p $nfdir/jetbrainsmono
unzip JetBrainsMono.zip
mv JetBrainsMono* $nfdir/jetbrainsmono/
rm JetBrainsMono.zip

fc-cache -f -v
