<!-- Heading -->
# Debian-Install-Scripts

#### These scripts are written to install various window managers and softwareon a Debian/Devuan system. They also have the ability to utilize sudo or doas... whatever you have installed and configured.

* **alacritty.sh** - installs the alacritty terminal with rustup and cargo since it's not in the debian repos yet.
* **android.sh** - simply installs adb, fastboot, sdk, and platform tools
* **brave-browser** - adds keys and repo stable release of brave browser since it isn't in the standard repos.
* **devour.sh** - clones git repo for devour, runs make and symlinks to ~/.local/bin
* **fonts.sh** - installs ubuntu fonts, yosemite sanfrancisco font, powerline fonts,and mscorefonts (mscorefonts is not available in Devuan repos)
* **git-i3gaps.sh** - i3-gaps is not available in the Debian/Devuan repos, this script installs the dependencies with apt build-dep, clones the repo and installs with meson and ninja. (not rested with display manager)
* **git-qtile.sh** - qtile is not available in the Debian/Devuan repos, this script builds it from source straight from GitHub. (not tested with display manager)
* **git-spectrwm.sh** - builds newest version of spectrwm from GitHub. (not tested with display manager)
* **gruvbox-gtk-theme.sh** - installs gruvbox material gtk theme from The Great McPain. [link](https://github.com/TheGreatMcPain/gruvbox-material-gtk "github link")
* **inconsolata-nerd-font.sh** - downloads inconsolata nerdfont, places it in ~/.local/share/fonts/nerdfonts/ and refreshes font-cache.
* **ld-doas.sh** - RUN AS ROOT! - downloads my doas.conf file, replaced my name with your username, and places it in /etc/doas.conf
* **ld-st.sh** - installs my build of st terminal.
* **lf.sh** - installs the lf terminal file manager from source since it is not in the debian repos yet.
* **librewolf.sh** - installs the librewolf web browser from source since it is not in the debian repos yet.
* **neovimplug.sh** - installs neovim from the repos, but also adds vimplug functionality.
* **plex-media-server.sh** - adds repo to /etc/apt/sources/list.d/plexmediaserver.list, adds apt keys, installs plex, and enables on boot.
* **steam.sh** - adds 32 bit architecture, runs updates, installs steam, mesa-vulkan-drivers and the necessary 32 bit mesa drivers.
* **ubuntu-nerd-font.sh** - downloads ubuntu nerdfont, places it in ~/.local/share/fonts/nerdfonts/ and refreshes font-cache.
* **virt-manager.sh** - installs qemu-kvm, libvirt dependencies, bridge-utils, and virt-manager. enables service at boot, starts service, adds user to libvirt group and libvirt-qemu group.
* **zfs.sh** - installs ZFS DKMS and ZFS utilities, probes kernel, enables zfs services, and starts services. ( this is for ZFS support, not for root partition)
* **zoom.sh** - installs zoom dependencies from apt repos, wget's latest zoom deb package and installs it.



you can see many of these scripts in action on my [youtube channel](https://youtube.com/c/linuxdabbler)
