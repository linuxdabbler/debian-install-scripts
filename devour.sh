#!/bin/bash

srcdir="$HOME/.local/src"
bindir="$HOME/.local/bin"

[ -x "$(command -v sudo)" ] && ld="sudo"
[ -x "$(command -v doas)" ] && [ -e /etc/doas.conf ] && ld="doas"

[ -x "$(command -v git)" ] || $ld apt install git -yy
[ -x "$(command -v make)" ] || $ld apt install make -yy

[ -d "$srcdir" ] || mkdir -p $srcdir

git clone https://github.com/salman-abedin/devour $srcdir/devour

make --directory $srcdir/devour

ln -sf $srcdir/devour/devour $bindir
