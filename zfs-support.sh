#!/bin/bash

# script template called from ld's nvim config

# Declare colors
red='\033[0;31m'
nc='\033[0m'
yellow='\033[1;33m'

runas=$(whoami)

if [ $runas != 'root' ]; then
	printf "${red}please execute this script as root, or use sudo or doas.${nc}\n" && exit 2
fi

check_contrib_repos="$(grep "contrib" /etc/apt/sources.list | wc -l)"

if [ $check_contrib_repos -lt '1' ]; then
	printf "${red} please enable ${yellow}contrib${red} repositories in ${yellow}/etc/apt/sources.list${nc}\n "
	printf "${red} then run ${yellow} sudo apt update -yy${red} and run the script again\n" && exit 1
fi


apt install linux-headers-"$(uname -r)" zfs-dkms zfs-zed zfsutils-linux -yy

modprobe zfs && zfs version

checker="$(zfs version | wc -l)"

if [ $checker -le '1' ]; then
	printf "${red}Kernel module did not install succesfully\n"
	printf "Please check the script for troubleshooting purposes${nc}\n"
else
	printf "${yellow}Enabling ZFS Services.${nc}\n"
		# Enable ZFS services
		systemctl enable zfs.target 
		systemctl enable zfs-import-cache 
		systemctl enable zfs-mount 
		systemctl enable zfs-import.target 
		systemctl enable zfs-import-scan 
		systemctl enable zfs-share 

		# Restart all ZFS services

		systemctl restart zfs-import-cache 
		systemctl restart zfs-import-scan 
		systemctl restart zfs-mount 
		systemctl restart zfs-share 
fi

