#!/bin/bash

red='\033[0;31m'
nc='\033[0m'

[ -x "$(command -v doas)" ] && [ -e /etc/doas.conf ] && ld="doas"
[ -x "$(command -v sudo)" ] && ld="sudo"
[ -x "$(command -v git)" ] || $ld apt install git -yy

$ld apt install --no-install-recommends gtk2-engines-murrine -yy

if [ -e /usr/share/themes/Gruvbox-Material-Dark/ ]; then
    printf "${red}Theme already installed...exiting ${nc}\n" && exit 1
else
    git clone https://github.com/TheGreatMcPain/gruvbox-material-gtk
    cp -r gruvbox-material-gtk/icons/ ~/.local/share/icons/
    $ld cp -r gruvbox-material-gtk/themes/Gruvbox-Material-* /usr/share/themes/
    $ld rm -r gruvbox-material-gtk/
    printf "${red}Theme assests copied to ~/.local/share/icons/ and /usr/share/themes/ ${nc}\n"
fi


